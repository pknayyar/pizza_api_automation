package com.sample.demo.cases;


import com.aventstack.extentreports.testng.listener.ExtentITestListenerClassAdapter;
import com.sample.demo.utils.Status;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.config.LogConfig;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.Listeners;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

import static io.restassured.RestAssured.given;
import static io.restassured.config.RestAssuredConfig.config;

@Listeners({ExtentITestListenerClassAdapter.class})
public abstract class BaseTest {

    public RequestSpecification spec;

    public RequestSpecification initSpec(){
        if(spec == null) {
            PrintStream fileOutPutStream = null;
            try {
                fileOutPutStream = new PrintStream(new File("pizza.log"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            RestAssured.config = config().logConfig(new LogConfig().defaultStream(fileOutPutStream));
            spec = new RequestSpecBuilder()
                    .setContentType(ContentType.JSON)
                    .setBaseUri("https://my-json-server.typicode.com/sa2225/demo")
                    .addFilter(new ResponseLoggingFilter())
                    .addFilter(new RequestLoggingFilter())
                    .build();
        }
        return spec;
    }

    protected JsonPath createResource(String path, Object bodyPayload, Status status) {
        return given()
                    .spec(initSpec())
                    .log().all()
                    .body(bodyPayload)
                .when()
                    .post(path)
                .then()
                    .statusCode(status.getCode())
                    .extract().jsonPath();
    }

    protected JsonPath updateResource(String path, Object bodyPayload, Status status) {
        return given()
                .spec(initSpec())
                .log().all()
                .body(bodyPayload)
                .when()
                .put(path)
                .then()
                .statusCode(status.getCode())
                .extract().jsonPath();
    }

    protected JsonPath getResource(String path, Status status) {
        return given()
                    .spec(initSpec())
                    .log().all(true)
                .when()
                    .get(path)
                .then()
                    .statusCode(status.getCode())
                    .extract().jsonPath();
    }

    protected JsonPath getResourceById(String path, String id, Status status) {
        return given()
                .spec(initSpec())
                .log().all()
                .param("id", id)
                .when()
                .get(path)
                .then()
                .statusCode(status.getCode())
                .extract().jsonPath();
    }

    protected JsonPath deleteResourceById(String path, String id, Status status) {
        return given()
                .spec(initSpec())
                .log().all()
                .pathParam("id", id)
                .when()
                .delete(path)
                .then()
                .statusCode(status.getCode())
                .extract().jsonPath();
    }
}
