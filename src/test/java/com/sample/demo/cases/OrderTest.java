package com.sample.demo.cases;

import com.sample.demo.model.Order;
import com.sample.demo.model.Pizza;
import com.sample.demo.utils.ResourceUtil;
import com.sample.demo.utils.Status;
import io.restassured.path.json.JsonPath;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class OrderTest extends BaseTest {

    private static final String ORDERS = "/orders";
    private static final String ORDERS_BY_ID = "/orders/{id}";    

    /*
     * 
     *  Test Case Description:  Creates a New Order
     *  Expected Result:  Status Code 200
     */
    @Test(groups = {"regression"})
    public void CreateOrderTest() {
        Order order = ResourceUtil.getOrder();
        JsonPath jsonPath = createResource(ORDERS, order, Status.CREATED);
        assertThat(jsonPath.getString("id")).isNotEmpty();
    }

    /*
     *  Test Case Description: Updates a Existing Order 
     *  Expected Result: 200 status code
     */
    @Test(groups = {"smoke", "regression"})
    public void UpdateOrderTest() {
        JsonPath responseOrders = getResourceById(ORDERS, ResourceUtil.getOrderId(), Status.OK);
        Order order = ResourceUtil.getObjectFromJsonPath(responseOrders, Order.class, 0);
        order.getItems().get(0).setPizza(ResourceUtil.getPizzas().get(0));
        JsonPath jsonPath = updateResource(ORDERS, order, Status.CREATED);
        assertThat(jsonPath.getString("id")).isNotEmpty();
    }

    /*
     *  Test Case Description: Create new order with multiple pizza orders (Boundry Test)
     *  Expected Result:  Status Code 200
     */
    @Test(groups = {"regression"})
    public void CreateOrderWithMultiplePizzasTest() {
        Order order = ResourceUtil.getOrder();
        Pizza pizza1 = new Pizza(ResourceUtil.getPizzas().get(0));
        Pizza pizza2 = new Pizza(ResourceUtil.getPizzas().get(1));
        Pizza pizza3 = new Pizza(ResourceUtil.getPizzas().get(2));
        List<Pizza> pizzas = new ArrayList<Pizza>();
        pizzas.add(pizza1);
        pizzas.add(pizza2);
        pizzas.add(pizza3);
        order.setItems(pizzas);
        JsonPath jsonPath = createResource(ORDERS, order, Status.CREATED);
        assertThat(jsonPath.getString("id")).isNotEmpty();
    }

    /*
     * Test Case Description: Create new order with max Toppings (Boundry Testing)
     * Expected Result:  Status Code 200
     */
    @Test(groups = {"regression"})
    public void CreateOrderWithMaxToppingsTest() {
        Order order = ResourceUtil.getOrder();
        order.getItems().get(0).setToppings(ResourceUtil.getToppings());
        JsonPath jsonPath = createResource(ORDERS, order, Status.CREATED);
        assertThat(jsonPath.getString("id")).isNotEmpty();
    }

    /*
     * Test Case Description : Create New Order with Invalid Pizza (Negative Test)
     * Expected Result:  Status Code 407 
     */
    @Test(groups = {"regression"})
    public void CreateOrderInvalidPizzaTest() {
        Order order = ResourceUtil.getOrder();
        order.getItems().get(0).setPizza(ResourceUtil.getInvalidPizza());
        JsonPath jsonPath = createResource(ORDERS, order, Status.INVALID_PIZZA);
        assertThat(jsonPath.getString("id")).isEmpty();
    }

    /*
     * Test Case Description: Create Order for a Pizza Not Specified (Negative Test)
     * Expected Result:  Status Code 408 
     */
    @Test(groups = {"regression"})
    public void CreateOrderPizzaIsNotSpecTest() {
        Order order = ResourceUtil.getOrder();
        order.getItems().get(0).setPizza(null);
        JsonPath jsonPath = createResource(ORDERS, order, Status.PIZZA_NOT_SPECIFIED);
        assertThat(jsonPath.getString("id")).isEmpty();
    }

    /*
     * Test Case Description: Create New order with incorrect # of toppings (negative test) 
     * Expected Result:  Status Code 406 
     */
    @Test(groups = {"regression"})
    public void CreateOrderIncorrectToppingsTest() {
        Order order = ResourceUtil.getOrder();
        order.getItems().get(0).setToppings(null);
        JsonPath jsonPath = createResource(ORDERS, order, Status.INCORRECT_NO_OF_TOPPINGS);
        assertThat(jsonPath.getString("id")).isEmpty();
    }

    /*
     * Test Case Description:  Validate all orders against DB store (db.json in resource directory)
     * Expected Result:  Status Code 200 
     */
    @Test(groups = {"smoke", "regression"})
    public void GetOrderTest() {
        JsonPath responseOrders = getResource(ORDERS, Status.OK);
        JsonPath dataStoreOrders = ResourceUtil.getOrdersFromDataStore();
        assertThat(responseOrders.getString("id")).isEqualTo(dataStoreOrders.getString("id"));
        assertThat(responseOrders.getString("items.pizza")).isEqualTo(dataStoreOrders.getString("items.pizza"));
        assertThat(responseOrders.getString("items.toppings")).isEqualTo(dataStoreOrders.getString("items.toppings"));
    }
    
    /*
     * Test Case Description:  Validate Given Order against DB (Note: We are validating against db Store via db.json)
     * Expected Result:  Status Code 200
     */
    @Test(groups = {"smoke", "regression"})
    public void GetOrderByIdTest() {
    	String id = ResourceUtil.getOrderId();
        JsonPath responseOrders = getResourceById(ORDERS, id, Status.OK);
        JsonPath dataStoreOrders = ResourceUtil.getOrdersFromDataStore();
        int jsonIndex = Integer.parseInt(id)-1;
        assertThat(responseOrders.getString("id["+jsonIndex+"]")).isEqualTo(dataStoreOrders.getString("id["+jsonIndex+"]"));
        assertThat(responseOrders.getString("items["+jsonIndex+"].pizza")).isEqualTo(dataStoreOrders.getString("items["+jsonIndex+"].pizza"));
        assertThat(responseOrders.getString("items["+jsonIndex+"].toppings")).isEqualTo(dataStoreOrders.getString("items["+jsonIndex+"].toppings"));
    }

    /*
     * Test Case Description:  Delete Order by ID 
     * Expected Result:  Status Code 200
     * Note: Currently API returns empty string when you delete a order. This should be updated 
     */
    @Test(groups = {"smoke", "regression"})
    public void DeleteOrderByIdTest() {
        deleteResourceById(ORDERS_BY_ID, ResourceUtil.getOrderId(), Status.OK);         
    }
}
