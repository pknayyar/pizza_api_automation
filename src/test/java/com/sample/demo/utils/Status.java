package com.sample.demo.utils;

public enum Status {
    OK(200),
    CREATED(201),
    INCORRECT_NO_OF_TOPPINGS(406),
    INVALID_PIZZA(407),
    PIZZA_NOT_SPECIFIED(408);
    private int code;
    Status(int code) {this.code = code;}

    public int getCode() {
        return code;
    }
}
