package com.sample.demo.utils;

import com.sample.demo.model.Order;
import com.sample.demo.model.Pizza;
import io.restassured.path.json.JsonPath;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class ResourceUtil {

    private static final String DB_STORE = "db.json";
    private static final String PROPERTY_STORE = "config.properties";
    private static Properties properties = null;

    public static Order getOrder() {
        Order order = new Order();
        Pizza pizza = new Pizza();
        pizza.setItem(getOrderId());
        pizza.setPizza(getPizzas().get(0));        
        pizza.setToppings(getToppings());
        order.setItems(Arrays.asList(pizza));
        return order;
    }

    public static <T> T getObjectFromJsonPath(JsonPath jsonPath, Class<T> responseClass, int index) {
        return jsonPath.getList("$", responseClass).get(index);
    }

    public static JsonPath getOrdersFromDataStore() {
        return getDataStore().setRoot("orders");
    }

    public static JsonPath getPizzasFromDataStore() {
        return getDataStore().setRoot("pizzas");
    }

    public static List<String> getPizzas() {
        return Arrays.asList(getPropertyStore().getProperty("valid.pizzas").split(","));
    }
    
    public static String getInvalidPizza() {
    	return getPropertyStore().getProperty("invalid.pizza");
    }
    
    public static String getOrderId() {
    	return getPropertyStore().getProperty("order.id");
    }

    public static List<String> getToppings() {
        return Arrays.asList(getPropertyStore().getProperty("toppings").split(","));
    }

    public static JsonPath getToppingsFromDataStore() {
        return getDataStore().setRoot("toppings");
    }

    private static JsonPath getDataStore() {
        return new JsonPath(getInputStream(DB_STORE));
    }

    private static Properties getPropertyStore() {
        if(properties == null) {
            try {
                properties = new Properties();
                properties.load(getInputStream(PROPERTY_STORE));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return properties;
    }

    private static InputStream getInputStream(String fileName) {
        return ResourceUtil.class.getClassLoader().getResourceAsStream(fileName);
    }
}
