## REST Assured Pizza API Automation Project

#### Project Pizza API Automation Overview
  
1. We are creating a API Automation framework for Ordering Pizza! using JAVA REST Assured
2. The Basic Endpoints are listed at https://my-json-server.typicode.com/sa2225/demo/
3. This Automaton covers 10 Test Scenarios
4. Testing covers foundational REST calls as well as boundry, negative, and happy path tests


#### Pizza API Automation Set Up

1. Import Public Repo into your respective IDE (Eclipse, IntelliJ, etc)
2. Run a Maven Build
3. Be Sure to run as a Clean Install
4. Once the Pizza Framework has completed Extent Report will be generated
5. Report will be located at {project_dir}/target/surefire-reports/ApiReport.html
6. Per Instructions the Extent report failures will indicate Actual Result vs Expected Result

#### Test Cases

1.CreateOrderTest - This Creates a New Order
2.UpdateOrderTest - Updates a existing Order
3.CreateOrderWithMultiplePizzasTest - Creates a New Order with Multiple Pizzas
4.CreateOrderWithMaxToppingsTest - Creates a Order with Max Toppings provided
5.CreateOrderInvalidPizzaTest - Attempting to Create invalid order
6.CreateOrderPizzaIsNotSpecTest - Creates Order for Pizza not specified
7.CreateOrderIncorrectToppingsTest - Creates Order with incorrect # of toppings
8.GetOrderTest - Validates Orders against all backend values
9.GetOrderByIdTest - Validates specific Order against DB by ID
10.DeleteOrderByIdTest - Delete an order by ID (Note: Currently API returns empty string when you delete a order. This should be updated)

#### Code Structure Overview

1.BaseTest.java -  Contains REST Assured abstraction calls 
2.OrderTest.java -  Contains a list of all Test Methods and Test Case description
3.Order.java -  Contains java mapping
4.ResourceUtlil.java -  Users our data driven config file and supporting methods
5.Config.properties -  Our data config file being passed in above ResourceUtil.java
6. Status.java -  Enum class where we store all of our Status Codes 
7. db.json -  Contains back end values for data validation
8. Extent Reporting - Extent.Properties and HTM-config.xml for reporting 
